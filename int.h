///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205 - Object Oriented Programming
/// Lab 02a - Datatypes
///
/// @file int.h
/// @version 1.0
///
/// Print the characteristics of the "short" and "unsigned short" datatypes.
///
/// @author @Daniel Luong <dluong>
/// @brief  Lab 02 - Datatypes - EE 205 - Spr 2021
/// @date   @January 22 2021
///////////////////////////////////////////////////////////////////////////////

extern void doInt();            /// Print the characteristics of the "int" datatype
extern void flowInt();          /// Print the overflow/underflow characteristics of the "int" datatype

extern void doUnsignedInt();    /// Print the characteristics of the "unsigned int" datatype
extern void flowUnsignedInt();  /// Print the overflow/underflow characteristics of the "unsigned int" datatype

